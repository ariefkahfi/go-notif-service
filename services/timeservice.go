package services

import (
	"io/ioutil"
	"strconv"
	"strings"
	"net/http"
	"time"
	"fmt"
	"notif-service/models"
	"github.com/go-shadow/moment"
	"github.com/go-redis/redis"
)




var (
	RedisClient = redis.NewClient(&redis.Options{
		Addr:"notif-db:6379",
		Password:"",
		DB:0,
	})
)


func SubscribeEvent() {
	pubsub := RedisClient.Subscribe("__keyevent@0__:expired")

	for c := range pubsub.Channel() {

		if !strings.Contains(c.Payload , "todo_deadline") {
			fmt.Println("INVALID_FORMAT")
			return
		}

		cSplit := strings.Split(c.Payload , ":")
		
		todoId := cSplit[1]
		todoUsername := cSplit[2]
		todoWhichEvent := cSplit[3]
		toInt, _ := strconv.Atoi(todoWhichEvent)
		
		switch toInt {
		case 1:
			todoWhichEvent = "MORNING_NOTIFY"
			break;
		case 2:
			todoWhichEvent = "AFTERNOON_NOTIFY"
			break;
		case 3:
			todoWhichEvent = "EVENING_NOTIFY"

			resp , _ := http.Get("http://rest-app:8989/RestTodo/api/todo/expired/"+todoId)
			rCloser := resp.Body
			b , _ := ioutil.ReadAll(rCloser)
			fmt.Println(b)
			break;
		}

		newEvent := model.NewEvent(todoUsername, todoId , todoWhichEvent)

		fmt.Println(newEvent)
	}
}


func setVal(redisdb *redis.Client , key string ,seconds int,  val interface{}) *redis.StringCmd {
	cmd := redis.NewStringCmd("SETEX",key,seconds , val)
	err := redisdb.Process(cmd)
	if err != nil {
		panic(err)
	}
	return cmd
}

func RemindMe(username , todoId string, dYear , dMonth , dDay int) {
	m := moment.New()
	m.SetDay(dDay)
	
	
	switch dMonth {
	case 1:
		m.SetMonth(time.January)
		break;
	case 2:
		m.SetMonth(time.February)
		break;
	case 3:
		m.SetMonth(time.March)
		break;
	case 4:
		m.SetMonth(time.April)
		break;
	case 5:
		m.SetMonth(time.May)
		break;
	case 6:
		m.SetMonth(time.June)
		break;
	case 7:
		m.SetMonth(time.July)
		break;
	case 8:
		m.SetMonth(time.August)
		break;
	case 9:
		m.SetMonth(time.September)
		break;
	case 10:
		m.SetMonth(time.October)
		break;
	case 11:
		m.SetMonth(time.November)
		break;
	case 12:
		m.SetMonth(time.December)
		break;
	}

	m.SetYear(dYear)


	yesterday := m.SubDay()
	yesterday.SetHour(6)
	yesterday.SetMinute(0)
	yesterday.SetSecond(0)
	yDiff1 := yesterday.FromNow()
	fmt.Println(yesterday.GetTime().String())

	yesterday.SetHour(12)
	yesterday.SetMinute(0)
	yesterday.SetSecond(0)
	yDiff2 := yesterday.FromNow()
	fmt.Println(yesterday.GetTime().String())



	yesterday.SetHour(18)
	yesterday.SetMinute(0)
	yesterday.SetSecond(0)
	yDiff3 := yesterday.FromNow()
	fmt.Println(yesterday.GetTime().String())


	fmt.Println(yDiff1.InSeconds(),yDiff2.InSeconds(),yDiff3.InSeconds())
	
	
	tKey1 := "todo_deadline:"+todoId+":"+username+":1"
	tKey2 := "todo_deadline:"+todoId+":"+username+":2"
	tKey3 := "todo_deadline:"+todoId+":"+username+":3"


	setVal(RedisClient,tKey1,yDiff1.InSeconds(),"morning_notify")
	setVal(RedisClient,tKey2,yDiff2.InSeconds(),"afternoon_notify")
	setVal(RedisClient,tKey3,yDiff3.InSeconds(),"evening_notify")


}


