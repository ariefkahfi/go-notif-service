FROM golang:1.8

WORKDIR /go/src/notif-service

COPY . .


RUN go get -d -v ./...
RUN go install -v ./...


EXPOSE 7777

CMD [ "go" , "run" , "server.go" ]

