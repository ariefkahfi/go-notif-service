package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"github.com/gorilla/mux"
	"notif-service/models"
	"notif-service/services"
)

func NotifService(w http.ResponseWriter, r *http.Request) {
	b , _ := ioutil.ReadAll(r.Body)

	var nBody model.NotifyBody
	json.Unmarshal(b , &nBody)

	services.RemindMe(nBody.Username, nBody.TodoId , nBody.TodoDeadlineYear , nBody.TodoDeadlineMonth , nBody.TodoDeadlineDay)

	b , _ = json.Marshal(nBody)
	w.Header().Set("Content-type","application/json")
	w.Write(b)
}


func DeleteNotif(w http.ResponseWriter, r *http.Request){
	rMaps := mux.Vars(r)
	
	rTodoId := rMaps["todoId"]
	rUsername := rMaps["username"]

	tKey1 := "todo_deadline:"+rTodoId+":"+rUsername+":1"
	tKey2 := "todo_deadline:"+rTodoId+":"+rUsername+":2"
	tKey3 := "todo_deadline:"+rTodoId+":"+rUsername+":3"

	_ , err := services.RedisClient.Del(tKey1,tKey2,tKey3).Result()

	w.Header().Set("Content-type","application/json")

	if err != nil {
		w.WriteHeader(500)
		b , _ := json.Marshal(
			map[string]interface{}{
				"code":500,
				"data":"CANNOT_PROCEED_THIS_REQUEST",
			},
		)
		w.Write(b)
		return
	}
	b , _ := json.Marshal(
		map[string]interface{}{
			"code":200,
			"data":"OK",
		},
	)
	w.Write(b)
}


func main(){
	r := mux.NewRouter()
	go services.SubscribeEvent()


	r.Path("/notif-service/api/notify/{todoId}/{username}").Methods("DELETE").HandlerFunc(DeleteNotif)
	r.Path("/notif-service/api/notify").Methods("POST").Headers("Content-type","application/json").HandlerFunc(NotifService)

	http.ListenAndServe(":7777",r)
}