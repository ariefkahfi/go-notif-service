package model



type NotifyBody struct {
	Username string `json:"username"`
	TodoId string `json:"todo_id"`
	TodoDeadlineDay int `json:"todo_deadline_day"`
	TodoDeadlineMonth int `json:"todo_deadline_month"`
	TodoDeadlineYear int `json:"todo_deadline_year"`
}

func NewNotifyBody(username , todoId string, tDeadlineDay , tDeadlineMonth, tDeadlineYear int) NotifyBody {
	return NotifyBody{
		Username:username,
		TodoId:todoId,
		TodoDeadlineDay:tDeadlineDay,
		TodoDeadlineMonth:tDeadlineMonth,
		TodoDeadlineYear:tDeadlineYear,
	}
}