package model


type Event struct {
	TodoId string `json:"todo_id"`
	TodoUsername string `json:"todo_username"`
	TodoWhichEvent string `json:"todo_which_event"`
}

func NewEvent(todoUsername , todoId , todoWhichEvent string) Event {
	return Event{
		TodoId:todoId,
		TodoUsername:todoUsername,
		TodoWhichEvent:todoWhichEvent,
	}
}